# Paprica commands

## Running locally

1. Install Docker or Podman (preferred)

2. Run interactive container
```
podman run -it registry.gitlab.com/gapi.petelin/o2-docker-commands/paprica
```

## Running on SLING

1. Download Docker/Singularity image
```
singularity pull -F --disable-cache  paprica.sif docker://registry.gitlab.com/gapi.petelin/o2-docker-commands/paprica
```
2. Run Paprica with Singularity
```
sbatch slingRun
```


